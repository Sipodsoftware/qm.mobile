### Requirements before build

- Copy keystore file inside *android/app*
- Create the file *gradle.properties* and add the following (replace ***** with the correct keystore password, alias and key password)
```txt
android.useDeprecatedNdk=true
QM_RELEASE_STORE_FILE=qm-release-key.keystore
QM_RELEASE_KEY_ALIAS=qm-key-alias
QM_RELEASE_STORE_PASSWORD=*******
QM_RELEASE_KEY_PASSWORD=*******
```


