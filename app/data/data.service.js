import {repository} from "./settings";

import MockRepositoryService from "./mock.repository";
import APIRepositoryService from "./api.repository";

const repositories = {
	mock: MockRepositoryService,
	api: APIRepositoryService
};

function list(resourceId, query) {
	return repositories[repository].list(resourceId, query);
}

export default {
	list
};