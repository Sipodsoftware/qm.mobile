import Mocks from "./mocks";

function list(resourceId, query) {
    return new Promise((resolve, reject) => {
	    var resource = getResource(resourceId, query);

	    if (resource) {
		    resolve(wrapToResponse(resource));
	    } else {
		    reject(new Error(`'${resourceId}' not found in mocks.`));
	    }
    });
}

function getResource(resourceId, query) {
	var resource = Mocks[resourceId].json();

	if (resource) {
		return doQuery(resource, query);
	} else {
		return null;
	}
}


function doQuery(resource, query) {
	var queriedResource = resource;

	if (query && Object.keys(query).length) {
		if (query.filter) {
			queriedResource = doFilter(resource, query.filter);
		}
	}

	return queriedResource;
}

function doFilter(resource, filter) {
	var newResource = [];

	resource.forEach(item => {
		Object.keys(item).forEach(propName => {
			if (item[propName] === filter[propName]) {
				newResource.push(item);
			}
		});
	});

	return newResource;
}

function wrapToResponse(resource) {
	return {
		json() {
			return resource;
		}
	}
}

export default {
	list
};