var data = [
	{
		"id": 1,
		"activityId": 1,
		"name": "Check room service quality",
		"location": "Any available on first floor",
		"status": "grey"
	},
	{
		"id": 2,
		"activityId": 1,
		"name": "Check hall service quality",
		"location": "Any available on second floor",
		"status": "green"
	},
	{
		"id": 3,
		"activityId": 1,
		"name": "Check kitchen service quality",
		"location": "Kings bedroom",
		"status": "grey"
	},
	{
		"id": 4,
		"activityId": 1,
		"name": "Check balcony service quality",
		"location": "Kings bedroom",
		"status": "green"
	},
	{
		"id": 5,
		"activityId": 1,
		"name": "Check dinning room service quality",
		"location": "Kings bedroom",
		"status": "grey"
	},
	{
		"id": 6,
		"activityId": 1,
		"name": "Check bathroom service quality",
		"location": "Kings bedroom",
		"status": "green"
	}
];

export default {
	json() {
		return data;
	}
};