var data = [
	{
		"id": 1,
		"name": "Checking service quality",
		"location": "Hotel Sun",
		"status": "green"
	},
	{
		"id": 2,
		"name": "Checking distributors work",
		"location": "Hotel Sun",
		"status": "grey"
	},
	{
		"id": 3,
		"name": "Checking service quality",
		"location": "Hotel Mercury",
		"status": "grey"
	}
];

export default {
	json() {
		return data;
	}
};