import activities from "./activities";
import sets from "./sets";
import tasks from "./tasks";

export default {
	activities,
	sets,
	tasks
};