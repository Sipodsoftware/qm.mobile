var data = [
	{
		"id": 1,
		"setId": 1,
		"name": "Is towel in the bathroom?",
		"status": "grey"
	},
	{
		"id": 2,
		"setId": 1,
		"name": "Is toilet paper on it's place",
		"status": "green"
	},
	{
		"id": 3,
		"setId": 1,
		"name": "Are there at least 3 soaps in the bathroom",
		"status": "grey"
	},
	{
		"id": 4,
		"setId": 1,
		"name": "Is refrigerator turned in power?",
		"status": "grey"
	},
	{
		"id": 5,
		"setId": 1,
		"name": "Are glasses in the bathroom cleaned ?",
		"status": "grey"
	},
	{
		"id": 6,
		"setId": 1,
		"name": "Are beds made well ?",
		"status": "green"
	},
	{
		"id": 7,
		"setId": 1,
		"name": "Are pencil and notebook on the table ?",
		"status": "grey"
	},
	{
		"id": 8,
		"setId": 1,
		"name": "Is TV remote control on the cabinet ?",
		"status": "grey"
	},
	{
		"id": 9,
		"setId": 1,
		"name": "Are keys of the strongbox in the strongbox lock ?",
		"status": "grey"
	},
	{
		"id": 10,
		"setId": 1,
		"name": "Are windows closed ? ",
		"status": "green"
	},
	{
		"id": 11,
		"setId": 1,
		"name": "Write down if you notice any additional issue in the room.",
		"status": "grey"
	},
	{
		"id": 12,
		"setId": 1,
		"name": "Open refrigerator and take a picture of it.",
		"status": "grey"
	},
	{
		"id": 13,
		"setId": 1,
		"name": "Dont disturb sign is left on the door lock ?",
		"status": "grey"
	},
	{
		"id": 14,
		"setId": 1,
		"name": "All lights works ?",
		"status": "green"
	},
	{
		"id": 15,
		"setId": 1,
		"name": "Empty Survey for guests are on the table ? ",
		"status": "grey"
	},
	{
		"id": 16,
		"setId": 1,
		"name": "Cabinet is empty ?",
		"status": "grey"
	}

];

export default {
	json() {
		return data;
	}
};