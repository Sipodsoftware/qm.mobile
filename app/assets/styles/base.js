import {StyleSheet} from "react-native";

export default StyleSheet.create({
	f1: {
		flex: 1
	},
	flexColumn: {
		flexDirection: "column"
	},
	flexRow: {
		flexDirection: "row"
	},
	paddingLeftRight20: {
		paddingLeft: 20,
		paddingRight: 20
	},
	alignCenter: {
		alignItems: "center"
	},
	justifyContent: {
		justifyContent: "center"
	}
});