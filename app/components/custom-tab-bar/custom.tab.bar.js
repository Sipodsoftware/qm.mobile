import React from "react";
import {
	StyleSheet,
	Text,
	View,
	TouchableOpacity,
} from 'react-native';

import styles from "./custom.tab.bar.style";

const CustomTabBar = React.createClass({

	propTypes: {
		goToPage: React.PropTypes.func,
		activeTab: React.PropTypes.number,
		tabs: React.PropTypes.array
	},

	arrowOpacity: 1,

	goLeft() {
		var goTo = this.props.activeTab - 1;
		if (goTo >= 0) {
			return goTo;
		}

		return this.props.activeTab;
	},

	goRight() {
		var goTo = this.props.activeTab + 1;
		if (goTo <= this.props.tabs.length - 1) {
			return goTo;
		}

		return this.props.activeTab;
	},

	render() {
		return (
			<View style={styles.tabBar}>
				<TouchableOpacity activeOpacity={this.arrowOpacity} style={styles.f1} onPress={() => this.props.goToPage(this.goLeft())}>
					<Text style={styles.arrowButton}>&lt;</Text>
				</TouchableOpacity>
				<View style={styles.f1}>
					<Text style={styles.pageIndicator}>{(this.props.activeTab + 1)}/{this.props.tabs.length}</Text>
				</View>
				<TouchableOpacity activeOpacity={this.arrowOpacity} style={styles.f1} onPress={() => this.props.goToPage(this.goRight())}>
					<Text style={styles.arrowButton}>&gt;</Text>
				</TouchableOpacity>
			</View>
		);
	}
});

export default CustomTabBar;