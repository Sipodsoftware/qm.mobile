import {StyleSheet} from "react-native";

export default StyleSheet.create({
	tabBar: {
		flexDirection: "row",
		justifyContent: "space-around",
		alignItems: "center",
		height: 50,
		backgroundColor: "#3399FF"
	},
	arrowButtonWrapper: {
		backgroundColor: "blue"
	},
	pageIndicator: {
		fontSize: 20,
		color: "#ffffff"
	},
	f1: {
		flex: 1,
		alignItems: "center"
	},
	arrowButton: {
		fontSize: 30,
		color: "#ffffff"
	}
});