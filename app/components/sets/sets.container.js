import React from "react";
import {connect} from "react-redux";

// Components
import Sets from "./sets";

// Actions
import {getTasks} from "../../actions/tasks";

//Routes
import {TASKS_LIST_ROUTE} from "../../navigator/routes";

const SetsContainer = React.createClass({
	//TODO: Refactor this after demo
	handleSetPress(setId) {
		console.log(setId);
		this.props.dispatch(getTasks({filter: {setId: 1}}));
		this.props.navigator.push(TASKS_LIST_ROUTE);
	},

	handleHeaderBackButton() {
		this.props.navigator.pop();
	},

	render() {
		const {sets} = this.props;

		return (
			<Sets handleSetPress={this.handleSetPress} sets={sets} handleHeaderBackButton={this.handleHeaderBackButton} headerTitle="Checking service quality" />
		);
	}
});

function mapStateToProps(state) {
	return {
		sets: state.setsReducer.sets
	}
}

export default connect(mapStateToProps)(SetsContainer);