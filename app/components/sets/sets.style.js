import {StyleSheet} from "react-native";

export default StyleSheet.create({
	wrapper: {
		backgroundColor: "white",
		flexDirection: "column",
		flex: 1
	},
	set: {
		alignItems: "center",
		justifyContent: "center",
		height: 50
	}
});