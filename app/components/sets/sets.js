import React from "react";
import {
	View,
	Text,
	TouchableHighlight
} from "react-native";

import Header from "../header/header";

import styles from "./sets.style";
import baseStyles from "../../assets/styles/base";

const Sets = React.createClass({
	statusIndicator(color) {
		return {
			width:15,
			height: 15,
			backgroundColor: color,
			borderRadius: 50
		}
	},
	render() {
		const {sets, handleHeaderBackButton, headerTitle} = this.props;
		return (
			<View style={styles.wrapper}>
				<Header handleBackButton={handleHeaderBackButton} title={headerTitle} />
				{
					sets.map((set) => {
						return (
							<TouchableHighlight
								key={set.id}
								underlayColor="lightgray"
								onPress={this.props.handleSetPress.bind(null, set.id)}
							>
								<View style={[styles.set, baseStyles.flexRow, baseStyles.paddingLeftRight20]}>
									<View style={baseStyles.f1}>
										<Text style={{fontWeight: "bold", color: "#1c1c1c", fontSize: 17}}>{set.name}</Text>
									</View>
									<View style={[this.statusIndicator(set.status)]}></View>
								</View>
							</TouchableHighlight>
						);
					})
				}
			</View>
		);
	}
});

export default Sets;