import {StyleSheet} from "react-native";

export default StyleSheet.create({
    wrapper: {
        flexDirection: "column",
        flex: 1,
        padding: 20
    },
    textInput: {
        height: 60,
        fontSize: 22
    },
    loginButton: {
        height: 40,
        backgroundColor: "#3399FF",
        alignItems: "center",
        justifyContent: "center"
    }
});