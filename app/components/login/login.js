import React from "react";
import {
    View,
    Text,
    TextInput,
    TouchableHighlight,
	Image
} from "react-native";

import Header from "../header/header";

import styles from "./login.style.js";
import baseStyles from "../../assets/styles/base";

const Login = React.createClass({
    render() {
        return (
	        <View style={baseStyles.f1}>
		        <Image
			        style={{marginLeft: -20, height: 100, resizeMode: "contain", alignItems: "center", backgroundColor: "#3399FF"}}
			        source={require("../../assets/images/logo.png")}
		        />
		        <View style={{backgroundColor: "#3399FF", alignItems: "flex-end", paddingRight: 20}}><Text style={{color: "white"}}>QM DEMO</Text></View>
		        <View style={styles.wrapper}>
			        <Header />
			        <TextInput
				        placeholder="Username or e-mail"
				        style={styles.textInput}
				        underlineColorAndroid="#3399FF"
			        />
			        <TextInput
				        placeholder="Password"
				        secureTextEntry={true}
				        style={styles.textInput}
				        underlineColorAndroid="#3399FF"
			        />
			        <TouchableHighlight
				        underlayColor="lightgray"
				        onPress={this.props.handleLoginButton}
			        >
				        <View style={styles.loginButton}>
					        <Text style={{color: "white", fontSize: 20}}>Login</Text>
				        </View>
			        </TouchableHighlight>
		        </View>
	        </View>
        );
    }
});

export default Login;