import React from "react";

// Routes
import {MAIN_ROUTE} from "../../navigator/routes";

// Components
import Login from "./login";

const LoginContainer = React.createClass({
    handleLoginButton() {
        this.props.navigator.push(MAIN_ROUTE);
    },
    render() {
        return (
            <Login handleLoginButton={this.handleLoginButton} />
        );
    }
});

export default LoginContainer;