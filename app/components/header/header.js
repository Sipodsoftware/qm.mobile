import React from "react";
import {
	View,
	Text,
	TouchableOpacity,
	StatusBar
} from "react-native";

import styles from "./header.style";
import baseStyles from "../../assets/styles/base";

const Header = React.createClass({
	render() {
		const {title, handleBackButton} = this.props;
		return (
			<View style={baseStyles.shadow}>
				<StatusBar
					backgroundColor="#3399FF"
					barStyle="light-content" />
				{(title && handleBackButton) ? (
					<View style={styles.headerWrapper}>
					<TouchableOpacity activeOpacity={1} onPress={handleBackButton} style={[{paddingLeft: 10}, baseStyles.justifyContent]}>
					<Text style={styles.text}>Back</Text>
					</TouchableOpacity>
					<View style={[baseStyles.f1, baseStyles.alignCenter, baseStyles.justifyContent, {paddingLeft: -40}]}>
					<Text style={[styles.text, {fontSize: 20}]}>{title}</Text>
					</View>
					<TouchableOpacity activeOpacity={1} style={[{paddingRight: 10}, baseStyles.justifyContent]}>

					</TouchableOpacity>
					</View>
				) : null}
			</View>
		);
	}
});

export default Header;