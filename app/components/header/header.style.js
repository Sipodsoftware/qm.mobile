import {StyleSheet} from "react-native";

export default StyleSheet.create({
	headerWrapper: {
		height: 50,
		flex: 1,
		flexDirection: "row",
		backgroundColor: "#3399FF"
	},
	text: {
		color: "#FFFFFF"
	}
});