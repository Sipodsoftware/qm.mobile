import React from "react";
import {connect} from "react-redux";

// Components
import Activities from "./activities";

// Actions
import {getActivities, getActivity} from "../../actions/activities";
import {getSets} from "../../actions/sets";

// Routes
import {SETS_LIST_ROUTE, MAIN_ROUTE} from "../../navigator/routes";

const ActivitiesContainer = React.createClass({
	componentDidMount() {
		this.props.dispatch(getActivities());
	},

	//TODO: DO refactor when real stuff needs to be made
	handleActivityPress(activityId) {
		this.props.dispatch(getSets({filter: {activityId: 1}}));
		this.props.navigator.push(SETS_LIST_ROUTE);
	},

	handleHeaderBackButton() {
		this.props.navigator.pop();
	},

	render() {
		const {activities} = this.props;

		return (
			<Activities handleActivityPress={this.handleActivityPress} activities={activities} handleHeaderBackButton={this.handleHeaderBackButton} headerTitle="Activities" />
		);
	}
});

function mapStateToProps(state) {
	return {
		activities: state.activitiesReducer.activities
	}
}

export default connect(mapStateToProps)(ActivitiesContainer);