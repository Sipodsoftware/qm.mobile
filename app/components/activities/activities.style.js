import {StyleSheet} from "react-native";

export default StyleSheet.create({
	wrapper: {
		backgroundColor: "white",
		flexDirection: "column",
		flex: 1
	},
	activity: {
		alignItems: "center",
		justifyContent: "center",
		height: 50,
		marginTop: 5,
		marginBottom: 5
	}
});