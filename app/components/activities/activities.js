import React from "react";
import {
	View,
	Text,
	TouchableHighlight
} from "react-native";

import Header from "../header/header";

import styles from "./activities.style";
import baseStyles from "../../assets/styles/base";

const Activities = React.createClass({
	statusIndicator(color) {
		return {
			width:15,
			height: 15,
			backgroundColor: color,
			borderRadius: 50
		}
	},
	render() {
		const {activities, handleHeaderBackButton, headerTitle} = this.props;
		return (
			<View style={styles.wrapper}>
				<Header handleBackButton={handleHeaderBackButton} title={headerTitle} />
				{
					activities.map((activity) => {
						return (
							<TouchableHighlight
								key={activity.id}
								underlayColor="lightgray"
								onPress={this.props.handleActivityPress.bind(null, activity.id)}
							>
								<View style={[styles.activity, baseStyles.flexRow, baseStyles.paddingLeftRight20]}>
									<View style={baseStyles.f1}>
										<Text style={{fontWeight: "bold", color: "#1c1c1c", fontSize: 17}}>{activity.name}</Text>
										<Text>{activity.location}</Text>
									</View>
									<View style={[this.statusIndicator(activity.status)]}></View>
								</View>
							</TouchableHighlight>
						);
					})
				}
			</View>
		);
	}
});

export default Activities;