import {StyleSheet} from "react-native";

export default StyleSheet.create({
	wrapper: {
		flexDirection: "column",
		justifyContent: "center",
		padding: 20,
		flex: 1,
		backgroundColor: "white"
	},
	option: {
		height: 50,
		backgroundColor: "#017CF2",
		alignItems: "center",
		justifyContent: "center"
	},
	optionText: {
		color: "white",
		fontSize: 20
	}
});