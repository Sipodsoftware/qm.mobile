import React from "react";
import {
	View,
	Text,
	TouchableHighlight
} from "react-native";

import Header from "../header/header";

import styles from "./main.styles";
import baseStyles from "../../assets/styles/base";

const Main = React.createClass({
	render() {
		const {handleHeaderBackButton, headerTitle} = this.props;
		return (
			<View style={baseStyles.f1}>
				<Header handleBackButton={handleHeaderBackButton} title={headerTitle} />
				<View style={styles.wrapper}>
					<TouchableHighlight style={{marginBottom: 20}} onPress={this.props.handleActivitiesButton}>
						<View style={styles.option}>
							<Text style={styles.optionText}>START ACTIVITIES</Text>
						</View>
					</TouchableHighlight>
					<TouchableHighlight>
						<View style={styles.option}>
							<Text style={styles.optionText}>CALENDAR</Text>
						</View>
					</TouchableHighlight>
				</View>
			</View>
		)
	}
});

export default Main;