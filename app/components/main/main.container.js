import React from "react";

// Routes
import {ACTIVITIES_LIST_ROUTE} from "../../navigator/routes";

// Components
import Main from "./main";

const MainContainer = React.createClass({
	handleHeaderBackButton() {
		this.props.navigator.pop();
	},
	handleActivitiesButton() {
		this.props.navigator.push(ACTIVITIES_LIST_ROUTE);
	},
	render() {
		return (
			<Main handleActivitiesButton={this.handleActivitiesButton} handleHeaderBackButton={this.handleHeaderBackButton} headerTitle="Home"/>
		);
	}
});

export default MainContainer;