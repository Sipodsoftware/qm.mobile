import React from "react";
import {connect} from "react-redux";

// Components
import Tasks from "./tasks";

// Routes
import {SWIPING_TASKS_ROUTE} from "../../navigator/routes";

const TasksContainer = React.createClass({
	handleTaskPress(initialTab) {
		this.props.navigator.push(Object.assign({}, SWIPING_TASKS_ROUTE, {initialTab}));
	},

	handleHeaderBackButton() {
		this.props.navigator.pop();
	},

	render() {
		const {tasks} = this.props;

		return (
			<Tasks handleTaskPress={this.handleTaskPress} tasks={tasks} handleHeaderBackButton={this.handleHeaderBackButton} headerTitle="Check room service quality" />
		);
	}
});

function mapStateToProps(state) {
	return {
		tasks: state.tasksReducer.tasks
	}
}

export default connect(mapStateToProps)(TasksContainer);