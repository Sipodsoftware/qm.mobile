import React from "react";
import {
	Text,
	TextInput,
	View,
	TouchableHighlight
} from "react-native";
import ScrollableTabView from "react-native-scrollable-tab-view";
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from "react-native-simple-radio-button";
import ImagePicker from "react-native-image-picker";

import SwipingTasksCustomTabBar from "../custom-tab-bar/custom.tab.bar.js";
import Header from "../header/header";

import baseStyles from "../../assets/styles/base";

const twoAnswers = [
	{label: "YES", value: 0 },
	{label: "NO", value: 1 }
];

const threeAnswers = [
	{label: "Yes", value: 0 },
	{label: "No", value: 1 },
	{label: "Not sure", value: 2 }
];

const options = {
	cameraType: 'back',
	mediaType: 'photo',
	quality: 1,
	allowsEditing: true,
	noData: false
};

const SwipingTasks = React.createClass({
	onSelectionHandler() {

	},
	launchCamera() {
		ImagePicker.launchCamera(options, (response)  => {

		});
	},
	render() {
		const {tasks, initialTab,  handleHeaderBackButton, headerTitle} = this.props;
		return (
			<View style={[baseStyles.f1, {backgroundColor: "white"}]}>
				<Header handleBackButton={handleHeaderBackButton} title={headerTitle} />
				<ScrollableTabView style={baseStyles.f1} initialPage={initialTab} renderTabBar={() => <SwipingTasksCustomTabBar />} tabBarPosition="bottom">

					<View style={[baseStyles.f1]} tabLabel="#1">
						<View style={[{padding: 20}, baseStyles.alignCenter]}>
							<Text style={{fontSize: 25, color: "black"}}>Is towel in the bathroom?</Text>
						</View>
						<View style={[baseStyles.alignCenter, {paddingBottom: 20}]}>
							<Text>Small description</Text>
						</View>
						<View style={[{paddingLeft: 20}, baseStyles.f1, baseStyles.justifyContent]}>
							<RadioForm
								radio_props={twoAnswers}
								initial={null}
								formHorizontal={false}
								labelHorizontal={true}
								buttonColor={"#2196f3"}
								onPress={(value) => {this.setState({value:value})}}
								animation={false}
							/>
						</View>
					</View>

					<View style={[baseStyles.f1]} tabLabel="#2">
						<View style={[{padding: 20}, baseStyles.alignCenter]}>
							<Text style={{fontSize: 25, color: "black"}}>Is toilet paper on it's place?</Text>
						</View>
						<View style={[baseStyles.alignCenter, {paddingBottom: 20}]}>
							<Text>Small description</Text>
						</View>
						<View style={[{paddingLeft: 20}, baseStyles.f1, baseStyles.justifyContent]}>
							<RadioForm
								radio_props={threeAnswers}
								initial={null}
								formHorizontal={false}
								labelHorizontal={true}
								buttonColor={"#2196f3"}
								animation={false}
								onPress={(value) => {this.setState({value:value})}}
							/>
						</View>
					</View>

					<View style={[baseStyles.f1]} tabLabel="#3">
						<View style={[{padding: 20}, baseStyles.alignCenter]}>
							<Text style={{fontSize: 25, color: "black"}}>Are there at least 3 soaps in the bathroom?</Text>
						</View>
						<View style={[baseStyles.alignCenter, {paddingBottom: 20}]}>
							<Text>Small description</Text>
						</View>
						<View style={[{paddingLeft: 20}, baseStyles.f1, baseStyles.justifyContent]}>
							<RadioForm
								radio_props={twoAnswers}
								initial={null}
								formHorizontal={false}
								labelHorizontal={true}
								buttonColor={"#2196f3"}
								animation={false}
								onPress={(value) => {this.setState({value:value})}}
							/>
						</View>
					</View>

					<View style={[baseStyles.f1]} tabLabel="#4">
						<View style={[{padding: 20}, baseStyles.alignCenter]}>
							<Text style={{fontSize: 25, color: "black"}}>Is refrigerator turned in power?</Text>
						</View>
						<View style={[baseStyles.alignCenter, {paddingBottom: 20}]}>
							<Text>Small description</Text>
						</View>
						<View style={[{paddingLeft: 20}, baseStyles.f1, baseStyles.justifyContent]}>
							<RadioForm
								radio_props={twoAnswers}
								initial={null}
								formHorizontal={false}
								labelHorizontal={true}
								buttonColor={"#2196f3"}
								animation={false}
								onPress={(value) => {this.setState({value:value})}}
							/>
						</View>
					</View>

					<View style={[baseStyles.f1]} tabLabel="#5">
						<View style={[{padding: 20}, baseStyles.alignCenter]}>
							<Text style={{fontSize: 25, color: "black"}}>Are glasses in the bathroom cleaned?</Text>
						</View>
						<View style={[baseStyles.alignCenter, {paddingBottom: 20}]}>
							<Text>Small description</Text>
						</View>
						<View style={[{paddingLeft: 20}, baseStyles.f1, baseStyles.justifyContent]}>
							<RadioForm
								radio_props={threeAnswers}
								initial={null}
								formHorizontal={false}
								labelHorizontal={true}
								buttonColor={"#2196f3"}
								animation={false}
								onPress={(value) => {this.setState({value:value})}}
							/>
						</View>
					</View>

					<View style={[baseStyles.f1]} tabLabel="#6">
						<View style={[{padding: 20}, baseStyles.alignCenter]}>
							<Text style={{fontSize: 25, color: "black"}}>Are beds made well?</Text>
						</View>
						<View style={[baseStyles.alignCenter, {paddingBottom: 20}]}>
							<Text>Small description</Text>
						</View>
						<View style={[{paddingLeft: 20}, baseStyles.f1, baseStyles.justifyContent]}>
							<RadioForm
								radio_props={twoAnswers}
								initial={null}
								formHorizontal={false}
								labelHorizontal={true}
								buttonColor={"#2196f3"}
								animation={false}
								onPress={(value) => {this.setState({value:value})}}
							/>
						</View>
					</View>

					<View style={[baseStyles.f1]} tabLabel="#7">
						<View style={[{padding: 20}, baseStyles.alignCenter]}>
							<Text style={{fontSize: 25, color: "black"}}>Are pencil and notebook on the table?</Text>
						</View>
						<View style={[baseStyles.alignCenter, {paddingBottom: 20}]}>
							<Text>Small description</Text>
						</View>
						<View style={[{paddingLeft: 20}, baseStyles.f1, baseStyles.justifyContent]}>
							<RadioForm
								radio_props={twoAnswers}
								initial={null}
								formHorizontal={false}
								labelHorizontal={true}
								buttonColor={"#2196f3"}
								animation={false}
								onPress={(value) => {this.setState({value:value})}}
							/>
						</View>
					</View>

					<View style={[baseStyles.f1]} tabLabel="#8">
						<View style={[{padding: 20}, baseStyles.alignCenter]}>
							<Text style={{fontSize: 25, color: "black"}}>Is TV remote control on the cabinet?</Text>
						</View>
						<View style={[baseStyles.alignCenter, {paddingBottom: 20}]}>
							<Text>Small description</Text>
						</View>
						<View style={[{paddingLeft: 20}, baseStyles.f1, baseStyles.justifyContent]}>
							<RadioForm
								radio_props={twoAnswers}
								initial={null}
								formHorizontal={false}
								labelHorizontal={true}
								buttonColor={"#2196f3"}
								animation={false}
								onPress={(value) => {this.setState({value:value})}}
							/>
						</View>
					</View>

					<View style={[baseStyles.f1]} tabLabel="#9">
						<View style={[{padding: 20}, baseStyles.alignCenter]}>
							<Text style={{fontSize: 25, color: "black"}}>Are keys of the strongbox in the strongbox lock?</Text>
						</View>
						<View style={[baseStyles.alignCenter, {paddingBottom: 20}]}>
							<Text>Small description</Text>
						</View>
						<View style={[{paddingLeft: 20}, baseStyles.f1, baseStyles.justifyContent]}>
							<RadioForm
								radio_props={threeAnswers}
								initial={null}
								formHorizontal={false}
								labelHorizontal={true}
								buttonColor={"#2196f3"}
								animation={false}
								onPress={(value) => {this.setState({value:value})}}
							/>
						</View>
					</View>

					<View style={[baseStyles.f1]} tabLabel="#10">
						<View style={[{padding: 20}, baseStyles.alignCenter]}>
							<Text style={{fontSize: 25, color: "black"}}>Are windows closed?</Text>
						</View>
						<View style={[baseStyles.alignCenter, {paddingBottom: 20}]}>
							<Text>Small description</Text>
						</View>
						<View style={[{paddingLeft: 20}, baseStyles.f1, baseStyles.justifyContent]}>
							<RadioForm
								radio_props={threeAnswers}
								initial={null}
								formHorizontal={false}
								labelHorizontal={true}
								buttonColor={"#2196f3"}
								animation={false}
								onPress={(value) => {this.setState({value:value})}}
							/>
						</View>
					</View>

					<View style={[baseStyles.f1]} tabLabel="#11">
						<View style={[{padding: 20}, baseStyles.alignCenter]}>
							<Text style={{fontSize: 25, color: "black"}}>Write down if you notice any additional issue in the room.</Text>
						</View>
						<View style={[baseStyles.alignCenter, {paddingBottom: 20}]}>
							<Text>Small description</Text>
						</View>
						<View style={{marginLeft: 20, marginRight: 20}}>
							<TextInput
								underlineColorAndroid="#3399FF"
								numberOfLines={5}
								placeholder="Describe room"
								multiline={true}
							/>
						</View>
					</View>

					<View style={[baseStyles.f1]} tabLabel="#12">
						<View style={[{padding: 20}, baseStyles.alignCenter]}>
							<Text style={{fontSize: 25, color: "black"}}>Open refrigerator and take a picture of it.</Text>
						</View>
						<View style={[baseStyles.alignCenter, {paddingBottom: 20}]}>
							<Text>Small description</Text>
						</View>
						<View style={[baseStyles.alignCenter, baseStyles.f1, {justifyContent: "flex-end"}, {paddingBottom: 20}]}>
							<TouchableHighlight style={{backgroundColor: "#FFB74F", paddingLeft: 20, paddingRight: 20, paddingTop: 10, paddingBottom: 10, borderRadius: 20}} onPress={this.launchCamera}>
								<Text style={{color: "#ffffff"}}>OPEN CAMERA</Text>
							</TouchableHighlight>
						</View>
					</View>

					<View style={[baseStyles.f1]} tabLabel="#13">
						<View style={[{padding: 20}, baseStyles.alignCenter]}>
							<Text style={{fontSize: 25, color: "black"}}>Dont disturb sign is left on the door lock?</Text>
						</View>
						<View style={[baseStyles.alignCenter, {paddingBottom: 20}]}>
							<Text>Small description</Text>
						</View>
						<View style={[{paddingLeft: 20}, baseStyles.f1, baseStyles.justifyContent]}>
							<RadioForm
								radio_props={threeAnswers}
								initial={null}
								formHorizontal={false}
								labelHorizontal={true}
								buttonColor={"#2196f3"}
								animation={false}
								onPress={(value) => {this.setState({value:value})}}
							/>
						</View>
					</View>

					<View style={[baseStyles.f1]} tabLabel="#14">
						<View style={[{padding: 20}, baseStyles.alignCenter]}>
							<Text style={{fontSize: 25, color: "black"}}>All lights works?</Text>
						</View>
						<View style={[baseStyles.alignCenter, {paddingBottom: 20}]}>
							<Text>Small description</Text>
						</View>
						<View style={[{paddingLeft: 20}, baseStyles.f1, baseStyles.justifyContent]}>
							<RadioForm
								radio_props={threeAnswers}
								initial={null}
								formHorizontal={false}
								labelHorizontal={true}
								buttonColor={"#2196f3"}
								animation={false}
								onPress={(value) => {this.setState({value:value})}}
							/>
						</View>
					</View>

					<View style={[baseStyles.f1]} tabLabel="#15">
						<View style={[{padding: 20}, baseStyles.alignCenter]}>
							<Text style={{fontSize: 25, color: "black"}}>Empty Survey for guests are on the table?</Text>
						</View>
						<View style={[baseStyles.alignCenter, {paddingBottom: 20}]}>
							<Text>Small description</Text>
						</View>
						<View style={[{paddingLeft: 20}, baseStyles.f1, baseStyles.justifyContent]}>
							<RadioForm
								radio_props={twoAnswers}
								initial={null}
								formHorizontal={false}
								labelHorizontal={true}
								buttonColor={"#2196f3"}
								animation={false}
								onPress={(value) => {this.setState({value:value})}}
							/>
						</View>
					</View>

					<View style={[baseStyles.f1]} tabLabel="#16">
						<View style={[{padding: 20}, baseStyles.alignCenter]}>
							<Text style={{fontSize: 25, color: "black"}}>Cabinet is empty?</Text>
						</View>
						<View style={[baseStyles.alignCenter, {paddingBottom: 20}]}>
							<Text>Small description</Text>
						</View>
						<View style={[{paddingLeft: 20}, baseStyles.f1, baseStyles.justifyContent]}>
							<RadioForm
								radio_props={threeAnswers}
								initial={null}
								formHorizontal={false}
								labelHorizontal={true}
								buttonColor={"#2196f3"}
								animation={false}
								onPress={(value) => {this.setState({value:value})}}
							/>
						</View>
					</View>
				</ScrollableTabView>
			</View>
		);
	}
});

export default SwipingTasks;