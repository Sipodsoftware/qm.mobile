import React from "react";
import {
	View,
	Text,
	TouchableHighlight,
	ScrollView
} from "react-native";

import Header from "../header/header";

import styles from "./tasks.style";
import baseStyles from "../../assets/styles/base";

const Tasks = React.createClass({
	statusIndicator(color) {
		return {
			width:15,
			height: 15,
			backgroundColor: color,
			borderRadius: 50
		}
	},
	render() {
		const {tasks, handleHeaderBackButton, headerTitle} = this.props;
		return (
			<View style={styles.wrapper}>
				<Header handleBackButton={handleHeaderBackButton} title={headerTitle} />
				<ScrollView>
					{
						tasks.map((task, index) => {
							return (
								<TouchableHighlight
									key={task.id}
									underlayColor="lightgray"
									onPress={this.props.handleTaskPress.bind(null, index)}
								>
									<View style={[styles.task, baseStyles.flexRow, baseStyles.paddingLeftRight20, {marginTop: 10, marginBottom: 10}]}>
										<View style={baseStyles.f1}>
											<Text style={{fontWeight: "bold", color: "#1c1c1c", fontSize: 17}}>{task.name}</Text>
										</View>
										<View style={[this.statusIndicator(task.status)]}></View>
									</View>
								</TouchableHighlight>
							);
						})
					}
				</ScrollView>
			</View>
		);
	}
});

export default Tasks;