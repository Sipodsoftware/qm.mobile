import React from "react";
import {connect} from "react-redux";

// Components
import SwipingTasks from "./swiping.tasks";

const SwipingTasksContainer = React.createClass({
	handleHeaderBackButton() {
		this.props.navigator.pop();
	},
	render() {
		const {tasks, route} = this.props;

		return (
			<SwipingTasks initialTab={route.initialTab} tasks={tasks} handleHeaderBackButton={this.handleHeaderBackButton} headerTitle="Task" />
		);
	}
});

function mapStateToProps(state) {
	return {
		tasks: state.tasksReducer.tasks
	};
}

export default connect(mapStateToProps)(SwipingTasksContainer);