import {createStore, applyMiddleware, combineReducers} from "redux";
import thunkMiddleware from "redux-thunk";

// Reducers
import activitiesReducer from "./reducers/activities";
import setsReducer from "./reducers/sets";
import tasksReducer from "./reducers/tasks";

var reducers = combineReducers({
	activitiesReducer,
	setsReducer,
	tasksReducer
});

// Exporting store
export default createStore(reducers, applyMiddleware(thunkMiddleware));