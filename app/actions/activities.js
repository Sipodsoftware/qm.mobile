import DataService from "../data/data.service";

// ACTION TYPES
export const RECEIVED_ACTIVITIES = "RECEIVED_ACTIVITIES";
export const RECEIVED_ACTIVITY = "RECEIVED_ACTIVITY";

// ACTION CREATORS
export function getActivities() {
	return (dispatch) => {
		DataService.list("activities")
			.then((response) => response.json())
			.then((data) => dispatch(receivedActivities(data)))
			.catch((err) => console.error(err));
	}
}

export function getActivity(id) {
	return (dispatch) => {
		// return axios.get(`http://192.168.1.101:3000/employees/${id}`).then(function (res) {
		//     return dispatch(receivedEmployee(res.data));
		// }).catch(function (err) {
		//     console.log(err);
		// });
	}
}

export function receivedActivities(activities) {
	return {
		type: RECEIVED_ACTIVITIES,
		payload: {
			activities
		}
	}
}

export function receivedActivity(activity) {
	return {
		type: RECEIVED_ACTIVITY,
		payload: {
			activity
		}
	}
}