import DataService from "../data/data.service";

// ACTION TYPES
export const RECEIVED_SETS = "RECEIVED_SETS";
export const RECEIVED_SET = "RECEIVED_SET";

// ACTION CREATORS
export function getSets(query) {
	return (dispatch) => {
		DataService.list("sets", query)
			.then((response) => response.json())
			.then((data) => dispatch(receivedSets(data)))
			.catch((err) => console.error(err));
	}
}

export function receivedSets(sets) {
	return {
		type: RECEIVED_SETS,
		payload: {
			sets
		}
	}
}