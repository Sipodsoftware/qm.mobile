import DataService from "../data/data.service";

// ACTION TYPES
export const RECEIVED_TASKS = "RECEIVED_TASKS";

// ACTION CREATORS
export function getTasks(query) {
	return (dispatch) => {
		DataService.list("tasks", query)
			.then((response) => response.json())
			.then((data) => dispatch(receivedTasks(data)))
			.catch((err) => console.error(err));
	}
}

export function receivedTasks(tasks) {
	return {
		type: RECEIVED_TASKS,
		payload: {
			tasks
		}
	}
}
