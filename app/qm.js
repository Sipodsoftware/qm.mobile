import React from "react";
import {Provider} from "react-redux";

import store from "./store";
import QMNavigator from "./navigator/qm.navigator";

const QM = React.createClass({
    render() {
        return (
            <Provider store={store}>
                <QMNavigator />
            </Provider>
        );
    }
});

export default QM;