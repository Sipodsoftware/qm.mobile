export const LOGIN_ROUTE = {
    id: "LOGIN_ROUTE"
};

export const MAIN_ROUTE = {
    id: "MAIN_ROUTE"
};

export const ACTIVITIES_LIST_ROUTE = {
	id: "ACTIVITIES_LIST_ROUTE"
};

export const SETS_LIST_ROUTE = {
	id: "SETS_LIST_ROUTE"
};

export const TASKS_LIST_ROUTE = {
	id: "TASKS_LIST_ROUTE"
};

export var SWIPING_TASKS_ROUTE = {
	id: "SWIPING_TASKS_ROUTE"
};

export const EMPLOYEES_LIST_ROUTE = {
    id: "EMPLOYEES_LIST_ROUTE"
};

export const EMPLOYEE_DETAILS_ROUTE = {
    id: "EMPLOYEE_DETAILS_ROUTE"
};