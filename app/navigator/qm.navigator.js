import React from "react";
import {Navigator, BackAndroid} from "react-native";

import {
    LOGIN_ROUTE,
    MAIN_ROUTE,
	ACTIVITIES_LIST_ROUTE,
	SETS_LIST_ROUTE,
	TASKS_LIST_ROUTE,
	SWIPING_TASKS_ROUTE
} from "./routes";

// Components
import LoginContainer from "../components/login/login.container";
import MainContainer from "../components/main/main.container";
import ActivitiesContainer from "../components/activities/activities.container";
import SetsContainer from "../components/sets/sets.container";
import TasksContainer from "../components/tasks/tasks.container";
import SwipingTasksContainer from "../components/tasks/swiping.tasks.container";

var _navigator;

const QMNavigator = React.createClass({
    navigatorRenderScene(route, navigator) {
        _navigator = navigator;

        switch (route.id) {
            case LOGIN_ROUTE.id: {
                return (<LoginContainer navigator={navigator} />)
            }
            case MAIN_ROUTE.id: {
                return (<MainContainer navigator={navigator} />)
            }
	        case ACTIVITIES_LIST_ROUTE.id: {
				return (<ActivitiesContainer navigator={navigator} />)
	        }
	        case SETS_LIST_ROUTE.id: {
		        return (<SetsContainer navigator={navigator} />)
	        }
	        case TASKS_LIST_ROUTE.id: {
		        return (<TasksContainer navigator={navigator} />);
	        }
	        case SWIPING_TASKS_ROUTE.id: {
				return (<SwipingTasksContainer navigator={navigator} route={route} />);
	        }
            default: {
                return (<LoginContainer navigator={navigator} />);
            }
        }
    },
    render() {
        return (
            <Navigator
                initialRoute={LOGIN_ROUTE}
                renderScene={this.navigatorRenderScene}
                configureScene={(route, routeStack) => Navigator.SceneConfigs.PushFromRight}
            />
        );
    }
});

// Handling back button press on Android
BackAndroid.addEventListener('hardwareBackPress', () => {
    if (_navigator.getCurrentRoutes().length === 1  ) {
        return false;
    }
    _navigator.pop();
    return true;
});

export default QMNavigator;