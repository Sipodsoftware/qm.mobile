import {RECEIVED_TASKS} from "../actions/tasks";

const initialState = {
	tasks: [],
	currentTask: null
};

function TasksReducer(state = initialState, action) {
	switch (action.type) {
		case RECEIVED_TASKS: {
			return Object.assign({}, state, {tasks: action.payload.tasks});
		}
		default: {
			return state;
		}
	}
}

export default TasksReducer;
