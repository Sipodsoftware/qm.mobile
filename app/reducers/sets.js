import {RECEIVED_SETS, RECEIVED_SET} from "../actions/sets";

const initialState = {
	sets: [],
	currentSet: null
};

function SetsReducer(state = initialState, action) {
	switch (action.type) {
		case RECEIVED_SETS: {
			return Object.assign({}, state, {sets: action.payload.sets});
		}
		case RECEIVED_SET: {
			return Object.assign({}, state, {currentSet: action.payload.set});
		}
		default: {
			return state;
		}
	}
}

export default SetsReducer;
