import {RECEIVED_ACTIVITIES, RECEIVED_ACTIVITY} from "../actions/activities";

const initialState = {
	activities: [],
	currentActivity: null
};

function ActivitiesReducer(state = initialState, action) {
	switch (action.type) {
		case RECEIVED_ACTIVITIES: {
			return Object.assign({}, state, {activities: action.payload.activities});
		}
		case RECEIVED_ACTIVITY: {
			return Object.assign({}, state, {currentActivity: action.payload.activity});
		}
		default: {
			return state;
		}
	}
}

export default ActivitiesReducer;
